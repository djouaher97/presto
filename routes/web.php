<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\RevisorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'homepage'])->name("welcome");
Route::get('/search', [PublicController::class, 'search'])->name("search");

// Route::get('/Accedi', [UserController::class, 'login'])->name("accedi");

// Route::get('/Registrati', [UserController::class, 'register'])->name("registrati");
// Route::post('/Registrati/submit', [UserController::class, 'userSubmit'])->name("user.submit");

Route::get('/inserisci-Annuncio', [UserController::class, 'addAnnunci'])->name("create");

Route::post('/inserisci-annuncio/create', [UserController::class, 'createAnnunci'])->name("add.create");



Route::get('/Articoli', [ArticleController::class, 'show'])->name("article");


//rotte per gli articoli
Route::get('/article/create', [ArticleController::class, 'create'])->name("article.create");
Route::post('/article/store', [ArticleController::class, 'store'])->name("article.store");



Route::get('/article/index', [ArticleController::class, 'index'])->name("article.index");
Route::get('/article/show/{article}', [ArticleController::class, 'show'])->name("article.show");



//rotte per il revisore ------------------------------------------------------------

Route::get('/revisor/home', [RevisorController::class, 'index'])->name("revisor.home");

Route::post('/revisor/article/{id}/accept', [RevisorController::class, 'accept'])->name("revisor.accept");
Route::post('/revisor/article/{id}/reject', [RevisorController::class, 'reject'])->name("revisor.reject");

Route::get('/articles/{category}', [PublicController::class, 'showCategory'])->name("article.category");

//ROTTA PER IL REVISORE
Route::get('/revisorform', [UserController::class, 'revisorForm'])->name("revisor.form");
Route::post('/revisorform/submit', [UserController::class, 'revisorSubmit'])->name("revisor.submit");

//ROTTA PER LE LINGUE
Route::post('/locale/{locale}', [PublicController::class, 'locale'])->name("locale");


//rotte per la dropzone 
Route::post('/articoli/images/upload', [ArticleController::class, 'imagesUpload'])->name("articoli.images.upload");
Route::delete('/articoli/images/remove', [ArticleController::class, 'removeImage'])->name("articoli.images.remove");
Route::get('/articoli/images', [ArticleController::class, 'getImages'])->name("articoli.images");