<?php

namespace App\Http\Controllers;

use App\Mail\AdminMail;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\UserController;

class UserController extends Controller
{
    

    protected function login(){
        return view("login");
    }

    protected function register(){
        return view("register");
    }

    protected function addAnnunci(){
        return view("create");
    }

    protected function logout(){
        return view("welcome");
    }

    protected function revisorForm(){
        return view("revisorform");
    }

    protected function revisorSubmit(Request $request){
        $user = $request->input('user');
        $email = $request->input('email');
        $description = $request->input('description');
        $forAdmin = compact('user', 'email', 'description');

        Mail::to('amministrazione@presto.com')->send(new AdminMail($forAdmin));
        
        return redirect(route("welcome"))->with('message', 'La tua richiesta è stata inoltrata, avrai risposta a breve');
        
    }
    
}
