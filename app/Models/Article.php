<?php

namespace App\Models;

use App\Models\User;
use App\Models\Article;
use App\Models\Category;
use App\Models\ArticleImage;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Article extends Model
{
    use Searchable;
    use HasFactory;

    protected $fillable =
    [
        'title',
        'price',
        'description',
        //'category',
        'img',
        //'data_insert',
    ];

     public function toSearchableArray()
    {
        $array = [ 
            'id' => $this->id,
            'title' => $this->title,
            'price' => $this->price,
    ];

        // Customize the data array...

        return $array;
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function categories(){
        return $this->belongsToMany(Category::class);
    }

        public function adImages()
    {
    return  $this->hasMany(ArticleImage::class);
    }

    static function ToBeRevisionedCount(){
        return Article::where('is_accepted', null)->count();
    }
    
}
