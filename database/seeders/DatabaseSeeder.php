<?php

namespace Database\Seeders;

use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $categories=[
            0=>['Auto', '/categoryimages/auto.png'],
            1=>['Moto', '/categoryimages/moto.png'],
            2=>['Telefonia', '/categoryimages/telefonia.png'],
            3=>['Elettronica', '/categoryimages/elettronica.png'],
            4=>['Videogiochi', '/categoryimages/videogiochi.png'],
            5=>['Libri', '/categoryimages/libri.png'],
            6=>['Abbigliamento', '/categoryimages/abbigliamento.png'],
            7=>['Immobili', '/categoryimages/immobili.png'],
            8=>['Lavoro', '/categoryimages/lavoro.png'],
            9=>['Animali', '/categoryimages/animali.png'],
        ];

        foreach ($categories as $category) {
            DB::table('categories')->insert(
                [
                    'name_category'=>$category[0],
                    'image_category'=>$category[1],
                    'created_at'=>Carbon::now(),
                    'updated_at'=>Carbon::now(),
                ]
                );
        }
    }
}
