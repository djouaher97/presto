<x-layout>

    <x-navbar></x-navbar>

    <x-slot name='title'> articoli</x-slot>

    {{-- <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>vediamo se funziona</h1>
                 <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="https://picsum.photos/200" alt="Card image cap">
                    <div class="card-body">
                      <h5 class="card-title">Nome: {{$article->name}}</h5>

                      @if(count($article->categories))
                      @foreach ($article->categories as $category)
                        <p>Categoria: {{$category->name_category}}</p>
                      @endforeach
                    @endif

                      <h5 class="card-title">Prezzo: {{$article->price}}</h5>
                      <h5 class="card-title">Data: {{$article->created_at->format('m/d/Y')}}</h5>
                      <p class="card-text">{{$article->description}}</p>
                      <a href="{{route("article.index")}}" class="btn btn-custom">Back</a>
                    </div>
                  </div>
            </div>

        </div>
    </div> --}}



    <h2 class="display-4 text-center mt-5">{{$article->title}}</h2>
    <div class="container mt-5">
      <div class="row justify-content-center align-items-center">
        <div class="col-12 col-md-7">
          {{-- <img src="https://picsum.photos/700/600" alt=""> --}}
          @foreach ($article->adImages as $image)
          <img class="rounded py-3 w-75" src="{{$image->getUrl(400, 250)}}" alt="">
          @endforeach 
        </div>
        <div class="col-12 col-md-5">
          @if(count($article->categories))
          @foreach ($article->categories as $category)
            <p class="text-center">Categoria: {{$category->name_category}}</p>
          @endforeach
        @endif
        <h4 class="text-center">Prezzo: {{$article->price}} euro</h4>
        <h4 class="text-center">Data: {{$article->created_at->format('m/d/Y')}}</h4>
        <p class="text-center lead ms-2">Descrizione: {{$article->description}}</p>
        </div>
      </div>
    </div>
    <div class="container text-center mt-5">
      
      <a href="{{route("article.index")}}" class="btn btn-custom text-white">Back</a>
    </div>


    <x-footer></x-footer>
</x-layout>