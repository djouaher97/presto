<nav class="navbar navbar-expand-lg navbar-light bg-light p-0">
  <div class="container-fluid shadow-navbar">
    <a class="navbar-brand ms-5" href="{{route('welcome')}}"><img src="{{Storage::url('img/logo_navbar.png')}}" alt=""></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <div class="container-fluid me-5">
        <div class="row align-items-center">
          <ul class="navbar-nav justify-content-end">
            @guest
            
            <li class="nav-item">
              <a class="nav-link fw-bold lead" aria-current="page" href="{{route("login")}}">{{ __('ui.sign_in') }}</a>
            </li>
            <li class="nav-item">
              <a class="nav-link fw-bold lead" href="{{route("register")}}">{{ __('ui.register') }}</a>
            </li>
            <li class="nav-item">
              <a class="nav-link fw-bold btn btn-custom-navbar ms-2 text-white lead" href="{{route("login")}}">{{ __('ui.announce_bar') }}</a>
            </li>
    
            @else
    
            <li class="nav-item">
              <a class="nav-link fw-bold lead" aria-current="page" href="{{route("article.index")}}">{{ __('ui.articles_nav') }}</a>
            </li>
            
    
            <li class="nav-item">
              <a class="nav-link fw-bold btn btn-custom-navbar ms-2 text-white" href="{{route("article.create")}}">{{ __('ui.announce') }}</a>
            </li>
    
           
  
            <div class="dropdown">
                <button class="btn dropdown-toggle text-secondary lead fw-bold fs-5 ms-3" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                  {{__('ui.welcome')}} {{Auth::user()->name}}<img src="https://img.icons8.com/ios-filled/30/fa314a/hand-peace--v1.png"/>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                @if (Auth::user()->is_revisor)
                  <li class="nav-item">
                    <a class="nav-link fw-bold lead" href="{{route('revisor.home')}}">
                    {{ __('ui.revisor') }}
                    
                    <p class="badge bg-warning text-dark">{{App\Models\Article::ToBeRevisionedCount()}}</p>
                    </a>
                  </li>
                @endif
                <li class="nav-item">
                  <a class="nav-link fw-bold lead" aria-current="page" href="{{route('revisor.form')}}">{{ __('ui.job_with_us') }}</a>
                </li>
    
                <li class="nav-item">
                  <a class="nav-link fw-bold lead" href="{{route('logout')}}"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">{{ __('ui.logout') }}</a>
                </li>
              </ul>
            </div>
            
    
              <form id="logout-form" action="{{route('logout')}}" method="POST" class="d-none">
              @csrf
              </form>
              @endguest
              <div class="dropdown justify-content-end ms-3 ">
                <button class="btn dropdown-toggle text-secondary lead fw-bold" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                  <img src="https://img.icons8.com/ios/40/fa314a/language.png"/>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                  <li class="nav-item">
                    @include('components.locale', ['lang' => 'it', 'nation' => 'italy'])
                  </li>

                  <li class="nav-item">
                    @include('components.locale', ['lang' => 'en', 'nation' => 'usa'])
                  </li>

                  <li class="nav-item">
                    @include('components.locale', ['lang' => 'es', 'nation' => 'spain'])
                  </li>
                </ul>
              </div>
    
    
          </ul>
        </div>
      </div>
      
    </div>
    </div>
  </div>
</nav>