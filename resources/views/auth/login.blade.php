<x-layout>
    <x-navbar></x-navbar>



   <div class="container">
       <div class="row justify-content-center">
           <h2 class="display-4 mt-5 text-center">Accedi</h2>

           @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
              </ul>
            </div>
          @endif

           <div class="col-12 col-md-6 mt-5">
            <img class="w-75" src="{{Storage::url('img/Mobile-login.png')}}" alt="">
           </div>
           <div class="col-12 col-md-6 mt-5">
            <form method="POST" action="{{route('login')}}">
              @csrf
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label lead">Indirizzo Email</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" name="email">
                  </div>
                <div class="mb-3">
                  <label for="exampleInputPassword1" class="form-label lead">Password</label>
                  <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                </div>
                <button type="submit" class="btn btn-custom">Accedi</button>
                <div class="mb-3">
                  <p class="lead">Non hai un account? <a href="{{route("register")}}">{{ __('ui.register') }}</a></p>
                </div>
              </form>
           </div>
       </div>
   </div>
</x-layout>