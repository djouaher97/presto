<?php

return [
    'category' => 'Esplora le nostre categorie',
    'search_articles' => "Trova il tuo prossimo acquisto",
    'what_find' => "Cosa cerchi?",
    'price' => "Prezzo",
    'last_announce' => "Ultimi Annunci",
    'sign_in' => "Accedi",
    'register' => "Registrati",
    'articles_nav' => "Articoli",
    'logout' => "Logout",
    'announce' => "Inserisci Annuncio",
    'job_with_us' => "Lavora con noi",
    'revisor' => "Home del revisore",
    'announce_bar' => "Inserisci Annuncio",
    'language'=>"Scegli la tua lingua",
    'welcome' => "Benvenuto",
    'access' => "Accedi",
    'account' => "Hai un account?"
];